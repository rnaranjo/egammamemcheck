
Asumming that you have the structure:
<br>
<br>
build/
source/
<br>
<br>
Inside build/ do
<br>
```asetup 21.2.55,AnalysisBase```<br>
```cmake -DCMAKE_BUILD_TYPE=Debug -DATLAS_PACKAGE_FILTER_FILE=../source/package_filters.txt ../source```<br>
(only use the package filter if you checkout athena as well, for modifying the packages)<br>
```make```<br>
```source x86_64-slc6-gcc62-dbg/setup.sh```<br>
<br>
<br>
Just run:
<br>
<br>
```runTest```
<br>
<br>
The name of the sample and location is hardcoded, so run on lxplus to access /eos