#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "PathResolver/PathResolver.h"
#include "AnaAlgorithm/AnaAlgorithmConfig.h"
#include "AnaAlgorithm/AnaAlgorithm.h"

#include "EgammaMemCheck/MemCheckAnalysis.h"  

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  gSystem->Exec (("rm -rf " + submitDir).c_str());
  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  std::string inputFilePath = PathResolverFindCalibDirectory("/eos/user/s/sabidi/testFiles/mc16e/mc16_13TeV.344235.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4lep_notau.deriv.DAOD_HIGG2D1.e5500_s3126_r10724_p3629/mc16_13TeV/");
  SH::ScanDir().filePattern("DAOD_HIGG2D1.15331303._000038.pool.root.1").scan(sh,inputFilePath);


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );
// this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, 10); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  EL::AnaAlgorithmConfig alg;
  //MemCheckAnalysis* alg = new MemCheckAnalysis();
  alg.setType ("MemCheckAnalysis");

  // set the name of the algorithm (this is the name use with
  // messages)
  alg.setName ("MemCheckAnalysis");

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit (job, submitDir);


  return 0;
}
