#ifndef GETMEMORYSIZE_H
#define GETMEMORYSIZE_H

void process_mem_usage(double& vm_usage, double& resident_set);

#endif // GETMEMORYSIZE_H

