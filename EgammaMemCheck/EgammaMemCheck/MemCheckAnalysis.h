#ifndef EGAMMAMEMCHECK_MEMCHECKANALYSIS_H
#define EGAMMAMEMCHECK_MEMCHECKANALYSIS_H 

#include <AnaAlgorithm/AnaAlgorithm.h>

//xAOD EDM include
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/Egamma.h"

// Tool include
//#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>

#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "xAODEgamma/PhotonxAODHelpers.h"

class MemCheckAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MemCheckAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

private:
  // Configuration, and any other types of variables go here.
  asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_elRescale;


};

#endif
