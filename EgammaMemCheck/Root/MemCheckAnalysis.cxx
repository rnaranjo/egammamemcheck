#include <AsgTools/MessageCheck.h>
#include <EgammaMemCheck/MemCheckAnalysis.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODEventInfo/EventInfo.h>
#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "xAODEgamma/PhotonxAODHelpers.h"
#include "EgammaMemCheck/GetMemoryUsage.h"
#include <iomanip>

MemCheckAnalysis :: MemCheckAnalysis(const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
//    m_elRescale("CP::EgammaCalibrationAndSmearingTool/EgammaMVACalib",this)
    m_elRescale("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool",this) 
{

}



StatusCode MemCheckAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_CHECK(m_elRescale.setProperty("randomRunNumber","344235" ));
  ANA_CHECK(m_elRescale.setProperty("ESModel", "es2017_R21_v1"));
  ANA_CHECK(m_elRescale.setProperty("ResolutionType", "SigmaEff90"));
  ANA_CHECK(m_elRescale.setProperty("decorrelationModel", "FULL_v1")); 


  
  return StatusCode::SUCCESS;
}



StatusCode MemCheckAnalysis :: execute ()
{
  
  double vm_usage     = 0;
  double resident_set = 0;
  process_mem_usage(vm_usage, resident_set);

  ANA_MSG_INFO("New Event");
  const xAOD::PhotonContainer *RecoPhoton = nullptr;
    if (!evtStore()->retrieve(RecoPhoton, "Photons").isSuccess()) {
      ATH_MSG_ERROR("Failed to retrieve photon container. Exiting.");
      return StatusCode::FAILURE;
    }

  for (auto currPhoton : *RecoPhoton) {
    ANA_MSG_INFO(" mem vm/rss " << std::setprecision(4) << vm_usage/1000. << "/" << resident_set/1000);
    xAOD::Photon photon =  *currPhoton;
    int extraLoops = 100;
        
    for (int iex = 0; iex < extraLoops; ++iex) {

      if(!m_elRescale->applyCorrection(photon)) {
        ATH_MSG_ERROR("Failed to apply MVA calibration. Exiting.");
        return StatusCode::FAILURE;          
      }

    }
  }
  

  return StatusCode::SUCCESS;
}



StatusCode MemCheckAnalysis :: finalize ()
{
  return StatusCode::SUCCESS;
}
